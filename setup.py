"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from importlib.util import find_spec
from pathlib import Path

from setuptools import setup, find_packages

here = Path(__file__).parent

# Get the long description from the README file
readme_path = here /'README.md'


def get_requirements():
    """Return requirements for project. Include aiofiles if on appropriate
    platform
    """
    requirements = ['numpy']
    return requirements


def find_package_data(packages, data_extensions=()):
    """Find data files for each package according to valid extension tuple"""
    package_data = {}

    for package_name in packages:
        spec = find_spec(package_name)

        results = []
        for path_str in spec.submodule_search_locations:
            as_path = Path(path_str)

            results.extend([str(p.relative_to(as_path))
                            for ext in data_extensions
                            for p in as_path.glob(f"**/*.{ext}")])
        package_data[package_name] = results
    return package_data


def find_packages_in_namespace(namespace, **kwargs):
    """Find packages in a namespace package

    :param namespace: name of namespace package
    """
    return [f"{namespace}.{p}" for p in find_packages(namespace, **kwargs)]


packages = find_packages_in_namespace("simulation", exclude=[])
package_data = find_package_data(packages)

setup(
    name='geant4_sim',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='0.0.3',

    description='GEANT4 simulation helper',
    long_description=readme_path.read_text(encoding='utf-8'),

    # The project's main homepage.
    url='https://gitlab.com/agoose77/geant4_sim',

    # Author details
    author='Angus Hollands',
    author_email='angus.hollands@outlook.com',

    # Choose your license
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.6',
    ],

    # What does your project relate to?
    keywords='geant4 monte carlo simulation',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=packages,

    # Alternatively, if you want to distribute just a my_module.py, uncomment
    # this:
    #   py_modules=["my_module"],

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=get_requirements(),

    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    extras_require={
    },

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    package_data=package_data,

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'run_geant4=simulation.__main__:main',
        ],
    },
)