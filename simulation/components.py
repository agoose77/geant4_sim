from logging import getLogger
from typing import Callable, Dict

from Geant4 import (G4VModularPhysicsList, G4VUserDetectorConstruction, G4UserEventAction)

from .detector import DetectorBuilder

FACTORY_TEMPLATE = "define_{name}"
logger = getLogger(__name__)


class PhysicsList(G4VModularPhysicsList):
    def __init__(self, cuts_factory: Callable[[G4VModularPhysicsList], Dict[str, float]]):
        super().__init__()
        self._cuts_factory = cuts_factory

    def SetCuts(self):
        cuts_dict = self._cuts_factory(self)
        for particle, value in cuts_dict.items():
            self.SetCutValue(value, particle)


class DetectorConstruction(G4VUserDetectorConstruction):
    def __init__(self, factory: Callable[[DetectorBuilder], None]):
        super().__init__()

        self._builder = DetectorBuilder()
        self._factory = factory

    def Construct(self):
        self._factory(self._builder)
        return self._builder.world_detector.physical


class EventResultAction(G4UserEventAction):
    def __init__(self, callback, child_action: G4UserEventAction = None):
        super().__init__()
        self._callback = callback
        self._child_action = child_action

        self._result_for_event = None

    def get_last_result(self):
        return self._result_for_event

    def BeginOfEventAction(self, event):
        if self._child_action is not None:
            self._child_action.BeginOfEventAction(event)

    def EndOfEventAction(self, event):
        self._result_for_event = self._callback(event)

        if self._child_action is not None:
            self._child_action.EndOfEventAction(event)
