import logging.config

logging_config = {
    'version': 1,
    'formatters': {
        'timestamp': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        }
    },
    'handlers': {
        'stdout': {
            'class': 'logging.StreamHandler',
            'formatter': 'timestamp',
            'level': logging.DEBUG
        }
    },
    'root': {
        'handlers': ['stdout'],
        'level': logging.DEBUG,
    }
}

logging.config.dictConfig(logging_config)

from .run import run_simulation, run_simulation
from .metrics import METRIC_TYPES
from .initialise import initialise_specification_from_namespace, load_specification_namespace
from .configuration import extract_kwargs_from_commandline, generate_parser
