from pathlib import Path
from weakref import ref, ReferenceType
from json import loads
from Geant4 import (G4ThreeVector, G4VisAttributes, G4VSensitiveDetector, G4LogicalVolume, G4VSolid, G4SDManager,
                    G4PVPlacement, G4NistManager, G4Box, G4SubtractionSolid, G4Tubs, G4RotationMatrix, deg,
                    G4Color, cm, m, mm)
from Geant4.CADMesh import CADMesh

NAME_TO_UNIT = {'cm': cm, 'm': m, 'mm': mm}


class Detector:
    def __init__(self, builder_ref: ReferenceType, physical):
        self._builder_ref = builder_ref
        self.physical = physical

    @property
    def name(self):
        return self.physical.GetName()

    @property
    def solid(self):
        return self.logical.GetSolid()

    @property
    def logical(self):
        return self.physical.GetLogicalVolume()

    def make_sensitive(self, sensitive_detector):
        self._builder_ref().add_sensitive_detector(sensitive_detector, self.logical)


class DetectorBuilder:
    def __init__(self):
        super().__init__()

        self.sensitive_detectors = []
        self.detectors = {}

        # Keep reference to rotation pointers
        self._rotations = []

    @property
    def world_detector(self):
        try:
            return next(d for d in self.detectors.values() if d.physical.GetMotherLogical() is None)
        except StopIteration:
            raise ValueError("No world detector is defined") from None

    def _create_cad_detector_from_scene_data(self, root_path, name, data, mother_logical=None):
        if not data.get('visible', True):
            vis_attributes = G4VisAttributes.GetInvisible()
        elif 'colour' in data:
            vis_attributes = G4VisAttributes(G4Color(*data['colour']))
        else:
            vis_attributes = None

        if "translate" in data:
            x, y, z, unit_name = data['translate']
            translation = G4ThreeVector(x, y, z) * NAME_TO_UNIT[unit_name]
        else:
            translation = None

        return self.create_from_cad(name, root_path / data['path'], data['material'], mother_logical,
                                    attributes=vis_attributes, translation=translation)

    def load_cad_scene(self, scene_path: Path):
        with open(scene_path) as f:
            scene_data = loads(f.read())

        world_names = [k for k, v in scene_data.items() if 'mother' not in v]
        assert len(world_names) == 1
        world_name = world_names[0]
        detector_world = self._create_cad_detector_from_scene_data(scene_path.parent, world_name,
                                                                   scene_data.pop(world_name))

        # Traverse from root detector to all leaf detectors
        parents = [detector_world]
        while parents:
            parent_detector = parents.pop()
            parent_name = str(parent_detector.name)

            detectors_with_parent = [n for n, v in scene_data.items() if v['mother'] == parent_name]
            for name in detectors_with_parent:
                detector = self._create_cad_detector_from_scene_data(scene_path.parent, name, scene_data.pop(name),
                                                                     parent_detector.logical)
                parents.append(detector)

    def _create_detector_from_solid(self, name: str, solid: G4VSolid, material_name: str,
                                    logical_mother: G4LogicalVolume, rotation: G4RotationMatrix = None,
                                    translation: G4ThreeVector = None, attributes: G4VisAttributes = None):
        if translation is None:
            translation = G4ThreeVector()

        material = G4NistManager.Instance().FindOrBuildMaterial(material_name)
        assert material, f"material {material_name!r} not defined"

        logical = G4LogicalVolume(solid, material, name)
        if attributes is not None:
            logical.SetVisAttributes(attributes)

        if rotation is not None:
            self._rotations.append(rotation)

        physical = G4PVPlacement(rotation, translation, logical, name, logical_mother, False, 0, True)
        detector = Detector(ref(self), physical)
        self.detectors[name] = detector
        return detector

    def add_sensitive_detector(self, sensitive_detector: G4VSensitiveDetector, logic_volume: G4LogicalVolume):
        logic_volume.SetSensitiveDetector(sensitive_detector)
        self.sensitive_detectors.append(sensitive_detector)
        G4SDManager.GetSDMpointer().AddNewDetector(sensitive_detector)

    def create_from_cad(self, name: str, path: Path, material_name: str, logical_mother: G4LogicalVolume,
                        rotation: G4RotationMatrix = None, translation: G4ThreeVector = None,
                        attributes: G4VisAttributes = None):
        path = path.resolve()
        mesh = CADMesh(str(path))
        solid = mesh.TessellatedMesh()
        return self._create_detector_from_solid(name, solid, material_name, logical_mother, rotation, translation,
                                                attributes)

    def create_cylinder(self, name: str, radius_inner: float, radius_outer: float, height: float, material_name: str,
                        logical_mother: G4LogicalVolume, start_phi: float = 0.0, dphi: float = 360 * deg,
                        rotation: G4RotationMatrix = None, translation: G4ThreeVector = None,
                        attributes: G4VisAttributes = None):
        solid = G4Tubs(name, radius_inner, radius_outer, height / 2, start_phi, dphi)
        return self._create_detector_from_solid(name, solid, material_name, logical_mother, rotation, translation,
                                                attributes)

    def create_cylindrical_shell(self, name: str, radius_outer: float, length_outer: float, radius_inner: float,
                                 length_inner: float, material_name: str, logical_mother: G4LogicalVolume,
                                 solid_translation: G4ThreeVector = None, start_phi: float = 0.0,
                                 dphi: float = 360 * deg,
                                 rotation: G4RotationMatrix = None, translation: G4ThreeVector = None,
                                 attributes: G4VisAttributes = None):
        solid_outer = G4Tubs(f"{name}_outer", 0.0, radius_outer, length_outer / 2, start_phi, dphi)
        solid_inner = G4Tubs(f"{name}_inner", 0.0, radius_inner, length_inner / 2, start_phi, dphi)
        solid = G4SubtractionSolid(name, solid_outer, solid_inner, None, solid_translation)
        return self._create_detector_from_solid(name, solid, material_name, logical_mother, rotation, translation,
                                                attributes)

    def create_box(self, name: str, width: float, length: float, height: float, material_name: str,
                   logical_mother: G4LogicalVolume, rotation: G4RotationMatrix = None,
                   translation: G4ThreeVector = None, attributes: G4VisAttributes = None):
        solid = G4Box(name, width / 2, length / 2, height / 2)
        return self._create_detector_from_solid(name, solid, material_name, logical_mother, rotation, translation,
                                                attributes)

    def create_box_shell(self, name: str, width_outer: float, length_outer: float, height_outer: float,
                         width_inner: float, length_inner: float, height_inner: float, material_name: str,
                         logical_mother: G4LogicalVolume, solid_translation: G4ThreeVector = None,
                         rotation: G4RotationMatrix = None, translation: G4ThreeVector = None,
                         attributes: G4VisAttributes = None):
        solid_outer = G4Box(f"{name}_outer", width_outer / 2, length_outer / 2, height_outer / 2)
        solid_inner = G4Box(f"{name}_inner", width_inner / 2, length_inner / 2, height_inner / 2)
        solid = G4SubtractionSolid(name, solid_outer, solid_inner, None, solid_translation)
        return self._create_detector_from_solid(name, solid, material_name, logical_mother, rotation, translation,
                                                attributes)
