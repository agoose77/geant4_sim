import sys
from logging import getLogger
from pathlib import Path
from runpy import run_path
from types import SimpleNamespace
from typing import Any, Dict

FACTORY_TEMPLATE = "define_{name}"
logger = getLogger(__name__)


def load_specification_namespace(path: Path) -> SimpleNamespace:
    """Load a SimpleNamespace object populated with the members of the given python module path"""
    from . import default_specification
    sys.path.insert(0, str(path.parent.resolve()))
    module_dict = run_path(path, init_globals=vars(default_specification))
    return SimpleNamespace(**module_dict)


def initialise_specification_from_namespace(namespace, seed: int, **configuration: Dict[str, Any]) -> SimpleNamespace:
    """Initialise a Geant4 simulation from a given namespace for a particular seed and configuration"""
    from Geant4 import G4NistManager, gRunManager, HepRandom, G4ParticleGun, G4GeneralParticleSource
    from g4py import NISTmaterials
    from g4py.PrimaryGenerator import PrimaryGeneratorAction
    from .components import PhysicsList, DetectorConstruction, EventResultAction

    # Setup user options
    args_processor = namespace.configure_simulation
    args_processor(**configuration)

    # Setup random engine
    random_engine_factory = namespace.define_random_engine
    random_engine = random_engine_factory()
    HepRandom.setTheEngine(random_engine)
    HepRandom.setTheSeed(seed)

    # Setup physics cuts
    physics_cuts_factory = namespace.define_physics_cuts
    physics_list = PhysicsList(physics_cuts_factory)

    # Setup physics constructors
    physics_constructors_factory = namespace.define_physics_constructors
    constructors = [*physics_constructors_factory(physics_list)]
    for constructor in constructors:
        physics_list.RegisterPhysics(constructor)

    # Setup materials
    NISTmaterials.Construct()
    nist_manager = G4NistManager.Instance()
    materials_factory = namespace.define_materials
    materials_factory(nist_manager)

    # Setup detectors
    try:
        detector_factory = namespace.define_detectors
    except KeyError:
        raise AttributeError("Specification must define detectors")
    detector_construction = DetectorConstruction(detector_factory)

    # Set physics & detectors
    gRunManager.SetUserInitialization(physics_list)
    gRunManager.SetUserInitialization(detector_construction)

    # Create particle gun or primary generator
    particle_gun = None
    particle_gun_configurer = None

    # Initialise before user actions
    gRunManager.Initialize()

    # User initialisation factory
    try:
        user_actions_factory = namespace.define_actions
    except AttributeError:
        pass
    else:
        user_actions = [*user_actions_factory(gRunManager)]

        for action in user_actions:
            gRunManager.SetUserAction(action)

    # Ensure this specification defines required result method
    try:
        get_detection_result = namespace.get_detection_result
    except AttributeError:
        raise AttributeError("Specification does not define `get_detection_result` function")

    # Overwrite the event action, and therefore wrap existing
    result_action = EventResultAction(get_detection_result, child_action=gRunManager.GetUserEventAction())
    gRunManager.SetUserAction(result_action)
    namespace.get_last_event_result = result_action.get_last_result

    # Create primary generator
    try:
        configure_gps = namespace.configure_general_particle_source
    except AttributeError:
        primary_generator = G4ParticleGun()
        if hasattr(namespace, 'configure_particle_gun'):
            namespace.configure_particle_gun(primary_generator)
    else:
        primary_generator = G4GeneralParticleSource()
        configure_gps(primary_generator)
    primary_generator_action = PrimaryGeneratorAction(primary_generator)
    gRunManager.SetUserAction(primary_generator_action)

    return SimpleNamespace(**vars(namespace), __initialiser_locals=locals())
