from argparse import ArgumentParser
from enum import Enum
from inspect import signature
from typing import Dict, Any, List


def generate_parser(configurer: callable) -> ArgumentParser:
    """Generate ArgumentParser for a given callable function"""
    parser = ArgumentParser()
    sig = signature(configurer)

    for name, param in sig.parameters.items():
        if issubclass(param.annotation, Enum):

            def param_type(value, param=param):
                return param.annotation[value]
        else:
            assert param.annotation is not param.empty
            param_type = param.annotation

        if param.default is param.empty:
            parser.add_argument(name, type=param_type)
        else:
            parser.add_argument(f'--{name}', default=param.default, type=param_type)
    return parser


def extract_kwargs_from_commandline(configurer: callable, argv: List[str]) -> Dict[str, Any]:
    """Inspect signature of configurer function and return dictionary of named arguments from argv"""
    parser = generate_parser(configurer)
    args = parser.parse_args(argv)
    return vars(args)
