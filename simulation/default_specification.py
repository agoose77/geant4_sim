from typing import Dict, Iterator

from Geant4 import (mm, G4VModularPhysicsList, G4EmLowEPPhysics, G4OpticalPhysics,
                    G4HadronPhysicsFTFP_BERT, G4DecayPhysics, G4RadioactiveDecayPhysics, Ranlux64Engine,
                    G4VPhysicsConstructor)


def configure_simulation():
    pass


def define_materials(nist_manager):
    pass


def define_physics_constructors(physics_list: G4VModularPhysicsList) -> Iterator[G4VPhysicsConstructor]:
    optical = G4OpticalPhysics()
    optical.SetWLSTimeProfile("delta")
    yield optical

    yield G4HadronPhysicsFTFP_BERT()
    yield G4EmLowEPPhysics()
    yield G4DecayPhysics()
    yield G4RadioactiveDecayPhysics()

    physics_list.SetVerboseLevel(0)


def define_physics_cuts(physics_list: G4VModularPhysicsList) -> Dict[str, float]:
    default_cut_value = 1 * mm
    return {'gamma': default_cut_value,
            'e-': default_cut_value,
            'e+': default_cut_value,
            'proton': default_cut_value}


def define_random_engine():
    return Ranlux64Engine()
