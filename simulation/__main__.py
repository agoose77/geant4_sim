from argparse import ArgumentParser
from pathlib import Path
from pprint import pprint
from random import getrandbits
from typing import Tuple

from .configuration import extract_kwargs_from_commandline
from .metrics import METRIC_TYPES
from .run import run_simulation


def get_metric_from_args(args) -> Tuple[str, int]:
    metric_args = [(k, getattr(args, k)) for k in METRIC_TYPES if getattr(args, k) is not None]
    assert len(metric_args) == 1, "Require one, and only one, terminating metric value"
    return metric_args.pop()


def main():
    from .initialise import initialise_specification_from_namespace, load_specification_namespace

    parser = ArgumentParser()
    parser.add_argument("spec", type=Path)
    parser.add_argument("-s", type=int, help='PRNG seed', default=getrandbits(32), dest='seed')
    parser.add_argument("-x", help="include macro", action='append', type=str, dest='macros')
    for metric in METRIC_TYPES:
        parser.add_argument(f"-{metric}", help=f'define termination criterion metric value', type=int)

    args, unknown_args = parser.parse_known_args()

    metric_type, metric_value = get_metric_from_args(args)
    namespace = load_specification_namespace(args.spec)
    kwargs = extract_kwargs_from_commandline(namespace.configure_simulation, unknown_args)

    specification = initialise_specification_from_namespace(namespace, args.seed, **kwargs)
    result = run_simulation(specification, metric_type, metric_value, args.macros)
    pprint(result)


if __name__ == "__main__":
    main()
