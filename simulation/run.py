from collections import defaultdict
from pathlib import Path
from typing import Dict, Any, List, Union


def run_macro(path: Union[Path, str]):
    """Run Geant4 UI macro from a given path"""
    from Geant4 import gUImanager
    if isinstance(path, Path):
        path = str(path)

    gUImanager.ExecuteMacroFile(path)


def run_simulation(simulation, metric_type: str, metric_limit: int, macro_paths: List[Path] = None) -> Dict[str, Any]:
    """Promise to call get_detection_result after each event"""
    from Geant4 import gRunManager
    for macro_path in (macro_paths or []):
        run_macro(macro_path)

    collection_detections = defaultdict(list)

    metrics = {
        'detections': 0,
        'events': 0
    }

    while metrics[metric_type] < metric_limit:
        gRunManager.BeamOn(1)

        result = simulation.get_last_event_result()

        metrics['events'] += 1
        if result is not None:
            metrics['detections'] += 1

            for k, v in result.items():
                collection_detections[k].append(v)

    return {'collections': dict(collection_detections), **metrics}
