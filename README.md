# Helper to run GEANT4 simulations
This package provides a framework with which to implement lightweight GEANT4 simulation definitions.
A simulation is defined using a 'specification file', which defines the geometry, sensitive detectors, materials, physics constructors, primary events and other components of a Geant4 simulation.
This file is then passed to the `run_geant4` program, which handles some of the boilerplate involved in defining a simulation.

Features such as the detector builder API make it simpler to define geometry and configure sensitive detectors, whilst the commandline API allows makes it easy to switch runtime settings using macros.

The user must define the detectors used in the simulation (see [simulation/detector.py](simulation/detector.py)). In this example, the `geant4_helpers` package (providing the `detectors` library) is used.
```python
from detector import SensitiveDetector

def define_detectors(detector_builder):
    detector_world = detector_builder.create_box("World", 30 * cm, 80 * cm, 30 * cm, "G4_AIR", logical_mother=None,
                                                 attributes=G4VisAttributes.GetInvisible())

    # Define your detectors here
    detector_photocathode = ...
    detector_crystal = ...
    
    # Now make them sensitive
    detector_photocathode.make_sensitive(SensitiveDetector("Photocathode"))
    detector_crystal.make_sensitive(SensitiveDetector("Crystal"))
```

Next, one must define the function which returns the result for each event. The result of this function should be a dictionary mapping a collection name to a result for the current event. The framework maintains a results list for each collection name.
```python
from detector import getHCFromVirtual, countOfType, getEDepArray, getPDGArray


def get_detection_result(event) -> Union[Dict[str, Any], None]:
    hc_of_this_event = event.GetHCofThisEvent()

    vhc_photocathode = get_hit_collection(hc_of_this_event, "Photocathode")
    vhc_crystal = get_hit_collection(hc_of_this_event, "Crystal")

    # Crystal energy deposits
    hc_crystal = getHCFromVirtual(vhc_crystal)
    not_photons = getPDGArray(hc_crystal) != 0
    total_e_dep = getEDepArray(hc_crystal)[not_photons].sum()

    # Optical photon photocathode "hits"
    hc_photocathode = getHCFromVirtual(vhc_photocathode)
    n_photocathode_hits = countOfType(hc_photocathode, "opticalphoton")

    if not n_photocathode_hits:
        return None

    return {'photocathode': n_photocathode_hits, 'crystal': total_e_dep}
```

By default, the following configuration functions are defined (see [simulation/default_specification.py](simulation/default_specification.py)):
```python
def configure_simulation():
    pass


def define_materials(nist_manager):
    pass


def define_physics_constructors(physics_list: G4VModularPhysicsList) -> Iterator[G4VPhysicsConstructor]:
    optical = G4OpticalPhysics()
    optical.SetWLSTimeProfile("delta")
    yield optical

    yield G4HadronPhysicsFTFP_BERT()
    yield G4EmLowEPPhysics()
    yield G4DecayPhysics()
    yield G4RadioactiveDecayPhysics()

    physics_list.SetVerboseLevel(0)


def define_physics_cuts(physics_list: G4VModularPhysicsList) -> Dict[str, float]:
    default_cut_value = 1 * mm
    return {'gamma': default_cut_value,
            'e-': default_cut_value,
            'e+': default_cut_value,
            'proton': default_cut_value}


def define_random_engine():
    return Ranlux64Engine()
```

In addition, one can also define custom user actions
```python
def define_actions(run_manager):
    yield UserAction1()
    yield UserAction2()
    # ...
```

A simulation may be configured by defining a configuration method, whose arguments are inspected in order to generate a commandline parser (when the simulation is run from the commandline helper `run_geant4`).
```python
def configure_simulation(arg1: type=default, ...):
    pass
```